FROM ubuntu-upstart

MAINTAINER Nathan Valentine <nrvale0@gmail.com>
ENV HOSTNAME ubuntu-pe-agent
ENV PATH /bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin
ADD buildutils /tmp/buildutils
RUN /tmp/buildutils/install puppet ubuntu
CMD ["/sbin/init"]
EXPOSE 22
